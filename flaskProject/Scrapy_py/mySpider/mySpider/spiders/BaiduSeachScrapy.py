import scrapy
import logging
from scrapy.selector import Selector
from mySpider.BaiduseachItem  import BaiduSeachEntity
import random
import time
class BaiduseachscrapySpider(scrapy.Spider):
    name = 'BaiduSeachScrapy'
    # allowed_domains = ['www.baidu.com']
    start_urls = ['https://www.baidu.com/s?rtt=1&bsst=1&cl=2&tn=news&word=888']
    def parse(self, response):
        logging.info("执行parse")
        divs = Selector(response).xpath('//div[@class="result-op c-container xpath-log new-pmd"]/div/h3/a')
        logging.info(divs)
        for div in divs:
            href=div.css('a::attr(href)').get()
            title = GetMiddleStr(div.get(), '<!--s-text-->', '<!--/s-text-->')
            logging.info(div)
            logging.info(title)
            logging.info(href)
            yield scrapy.Request(href, callback=self.get_message)
        # next_page_url = response.xpath('//*[@id="page"]/div/a[10]').css('a ::attr(href)').get()
        next_page_url="https://www.baidu.com/s?rtt=1&bsst=1&cl=2&tn=news&ie=utf-8&word=888&x_bfe_rqs=03E80&x_bfe_tjscore=0.000000&tngroupname=organic_news&newVideo=12&rsv_dl=news_b_pn&pn=20"
        logging.info("下页地址: https://www.baidu.com"+next_page_url)
        time.sleep(30)
        yield scrapy.Request("https://www.baidu.com"+next_page_url, callback=self.parse)
    def get_message(self,response):
        logging.info("执行get_message()")
        item = BaiduSeachEntity()
        ccc=random.randint(10, 100)
        item['title']='标题'+str(ccc)
        item['link'] = 'www.baidu.com'+str(ccc)
        yield item






def GetMiddleStr(content,startStr,endStr):
  startIndex = content.index(startStr)
  if startIndex>=0:
    startIndex += len(startStr)
  endIndex = content.index(endStr)
  return content[startIndex:endIndex]