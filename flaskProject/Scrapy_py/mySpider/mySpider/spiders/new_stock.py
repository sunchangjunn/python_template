import scrapy
import logging
logging.basicConfig(level=logging.INFO, format='[%(asctime)s] [%(levelname)s %(filename)s:%(lineno)d]--> %(message)s')
class StockSpider(scrapy.Spider):
    name = 'stock' #定义爬虫名称
    allowed_domains = ['quote.stockstar.com'] #定义爬虫域
    # start_urls = ['http://quote.stockstar.com/stock/ranklist_a_3_1_1.html']
    start_urls = ['https://www.qiushibaike.com/text/']

    def parse(self, response):
        logging.info(response)
        # 解析作者的名称和段子内容
        div_list = response.xpath('//div[@class="article block untagged mb15 typs_long"]')
        # xpath返回的是列表，但是列表元素一定是selector类型的对象
        author = div_list.xpath('./div[1]/a[2]/h2/text()')[0].extract()
        # 列表调用了extract之后，表示将列表中每一个selector对象中data对应的字符串提取了出来
        content = div_list.xpath('./a/div//text()').extract()  # 将字符串提取出来后，存进的仍一个列表
        # 将列表转字符串
        content = ''.join(content)
        print(author, content)



    #定义开始爬虫链接
    # def parse (self, response) : #撰写爬虫逻辑
    #     logging.info(response)
    #     page = int (response.url.split("_")[-1].split(".")[0])#抓取页码
    #     item_nodes = response.css('#datalist tr')
        # for item_node in item_nodes:
        #     #根据item文件中所定义的字段内容，进行字段内容的抓取
        #     item_loader = StockstarItemLoader(item=StockstarItem(), selector = item_node)
        #     item_loader.add_css("code", "td:nth-child(1) a::text")
        #     item_loader.add_css("abbr", "td:nth-child(2) a::text")
        #     item_loader.add_css("last_trade", “td:nth-child(3) span::text")
        #     item_loader.add_css("chg_ratio", "td:nth-child(4) span::text")
        #     item_loader.add_css("chg_amt", "td:nth-child(5) span::text")
        #     item_loader.add_css("chg_ratio_5min","td:nth-child(6) span::text")
        #     item_loader.add_css("volumn", "td:nth-child(7)::text")
        #     item_loader.add_css ("turn_over", "td:nth-child(8) :: text")
        #     stock_item = item_loader.load_item()
        #     yield stock_item
        #     if item_nodes:
        #         next_page = page + 1
        #         next_url = response.url.replace ("{0}.html".format (page) , "{0}.html".format(next_page))
        #         yield scrapy.Request(url=next_url, callback=self.parse)