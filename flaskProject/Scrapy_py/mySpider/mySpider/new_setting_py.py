from scrapy.exporters import JsonLinesItemExporter #默认显示的中文是阅读性较差的Unicode字符
#需要定义子类显示出原来的字符集（将父类的ensure_ascii属性设置为False即可）
class CustomJsonLinesItemExporter(JsonLinesItemExporter):
    def __init__(self, file, **kwargs):
        super (CustomJsonLinesItemExporter, self).__init__(file, ensure_ascii=False, **kwargs)
    #启用新定义的Exporter类\
    FEED_EXPORTERS = {
        'json':'stockstar.settings.CustomJsonLinesItemExporter',
    }
    ...
    #Configure a delay for requests for the same website (default: 0)
    #See http:IIscrapy.readthedocs.org/en/latest/topics/settings.html#download-delay
    #See also autothrottle settings and docs DOWNLOAD DELAY = 0.25