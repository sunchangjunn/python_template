import scrapy

class BaiduSeachEntity (scrapy.Item) : # 建立相应的字段
    title = scrapy.Field()  # 标题
    link = scrapy.Field()  # 连接
