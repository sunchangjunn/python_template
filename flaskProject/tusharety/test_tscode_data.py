#code列表处理
import pandas as pd
import tushare as ts
from sqlalchemy import create_engine
import numpy as np
import logging

engine_ts = create_engine('mysql://root:Scj19890606@129.204.84.140:3306/tushare_code?charset=utf8',pool_recycle=3600,pool_size=10, max_overflow=20)
def test_profits_data():
    logging.info("开始任务")
    pro = ts.pro_api('e62b7a575e89479c41044bfc5d276ed91dd7def0be197bfa2d4d5aca')
    data = pro.stock_basic(exchange='', list_status='L', fields='ts_code,exchange,is_hs,symbol,name,area,industry,fullname,enname,market,curr_type,'
                                                                'list_status,list_date,delist_date')
    # res = data.to_sql('ts_codes', engine_ts, index=False, if_exists='append', chunksize=10000)
    # logging.info(res)

def test_hs_const():
    logging.info("开始")
    pro = ts.pro_api('e62b7a575e89479c41044bfc5d276ed91dd7def0be197bfa2d4d5aca')
    # 获取沪股通成分
    sh = pro.hs_const(hs_type='SH')
    # 获取深股通成分
    sz = pro.hs_const(hs_type='SZ')
    #合并pandas
    frames = [sh, sz]
    result = pd.concat(frames)
    # result.to_sql('tscode_hsgt', engine_ts, index=False, if_exists='append', chunksize=10000)


def test_stock_company():
    logging.info("开始任务")
    pd.set_option('display.width', 1000)
    pd.set_option('display.max_columns', 100)
    pro = ts.pro_api('8da5a360251a9cc6ff2f8cd1b937728567a62d427562bbaf54f2659a')
    sql_select_codes = "select *  from `ts_codes`"
    codes_df = pd.read_sql_query(sql_select_codes, engine_ts)
    codes_df.set_index("ts_code", inplace=True)
    logging.info(codes_df)
    df = pro.stock_company( fields='ts_code,exchange,chairman,manager,secretary,reg_capital,setup_date,province,city,introduction,website,email,office,employees,main_business,business_scope')
    df.set_index("ts_code", inplace=True)
    logging.info(df)

    result_df = pd.concat([codes_df['name'], df], axis=1)
    logging.info(result_df)
    result_df.reset_index()
    # result_df.to_sql('codes_message11', engine_ts, index=False, if_exists='append', chunksize=50000)

    for i in range(len(df)):


        logging.info(df.iloc[i].loc['chairman'])
        df.iloc[i]['name'] = df.iloc[i].loc['chairman']
        logging.info(df.iloc[i])
        break
    logging.info(df)

    # for elemint in df.values:
    #     logging.info(elemint)
    #     # logging.info(type(codes_df['name']))
    #     # logging.info(codes_df['name'].index)
    #     # # logging.info(codes_df.loc(elemint[0]))
    #     break

def test_trade_cal():
    logging.info("开始任务")
    pd.set_option('display.width', 1000)
    pd.set_option('display.max_columns', 100)
    pro = ts.pro_api('8da5a360251a9cc6ff2f8cd1b937728567a62d427562bbaf54f2659a')
    df = pro.trade_cal(exchange='', start_date='20210101', end_date='20220101',fields='exchange,cal_date,is_open,pretrade_date')
    logging.info(df)
    df.to_sql('trade_cal', engine_ts, index=False, if_exists='append', chunksize=10000)





    # res = df.to_sql('stock_company', engine_ts, index=False, if_exists='append', chunksize=50000)


