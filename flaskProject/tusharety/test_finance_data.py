#利润
#!/usr/bin/python
import time
import pandas as pd
import tushare as ts
from sqlalchemy import create_engine
import numpy as np
import logging


engine_ts = create_engine('mysql://root:Scj19890606@129.204.84.140:3306/tushare_code?charset=utf8',pool_recycle=3600,pool_size=10, max_overflow=20)


def test_profits_data():
    logging.info("开始任务")
    pro = ts.pro_api('8da5a360251a9cc6ff2f8cd1b937728567a62d427562bbaf54f2659a')
    df = pro.income(ts_code='600000.SH', start_date='20180101', end_date='20180730',
                    fields='ts_code,ann_date,f_ann_date,end_date,report_type,comp_type,basic_eps,diluted_eps')
    res = df.to_sql('profits', engine_ts, index=False, if_exists='append', chunksize=5000)
    logging.info(df)
def test_for_select_all_data():
    logging.info("开始任务")
    pro = ts.pro_api('8da5a360251a9cc6ff2f8cd1b937728567a62d427562bbaf54f2659a')
    sql="select ts_code from  ts_codes"
    codes=pd.read_sql_query(sql,engine_ts)
    i = 0;
    for code in codes.values:
        logging.info(code[0])
        logging.info(type(code))
        df = pro.income(ts_code=code[0], start_date='20180101', end_date='20190101',fields='ts_code,report_type,comp_type,ann_date,f_ann_date,end_date,basic_eps,diluted_eps,total_revenue,revenue,int_income,prem_earned,comm_income,n_commis_income,n_oth_income,n_oth_b_income,prem_income,out_prem,une_prem_reser,reins_income,n_sec_tb_income,n_sec_uw_income,n_asset_mg_income,oth_b_income,fv_value_chg_gain,invest_income,ass_invest_income,forex_gain,total_cogs,oper_cost,int_exp,comm_exp,biz_tax_surchg,sell_exp,admin_exp,fin_exp,assets_impair_loss,prem_refund,compens_payout,reser_insur_liab,div_payt,reins_exp,oper_exp,compens_payout_refu,insur_reser_refu,reins_cost_refund,other_bus_cost,operate_profit,non_oper_income,non_oper_exp,nca_disploss,total_profit,income_tax,n_income,n_income_attr_p,minority_gain,oth_compr_income,t_compr_income,compr_inc_attr_p,compr_inc_attr_m_s,ebit,ebitda,insurance_exp,undist_profit,distable_profit,update_flag')
        res = df.to_sql('profits', engine_ts, index=False, if_exists='append', chunksize=20000)
        time.sleep(2)
        # i = i + 1
        # logging.info(df)
        # if(i>=10):
        #     break
    logging.info("结束")





