#负债
import pandas as pd
import tushare as ts
from sqlalchemy import create_engine
import numpy as np
import logging


engine_ts = create_engine('mysql://root:Scj19890606@129.204.84.140:3306/tushare_code?charset=utf8',pool_recycle=3600,pool_size=10, max_overflow=20)


def test_profits_data():
    logging.info("开始任务,获取利润表")
    pro = ts.pro_api()
    pro = ts.pro_api('8da5a360251a9cc6ff2f8cd1b937728567a62d427562bbaf54f2659a')
    df = pro.balancesheet(ts_code='600000.SH', start_date='20180101', end_date='20180730',
                          fields='ts_code,ann_date,f_ann_date,end_date,report_type,comp_type,cap_rese')
    res = df.to_sql('bebt', engine_ts, index=False, if_exists='append', chunksize=10000)
    logging.info(df)


def test_read_mysql():
    logging.info("开始读取数据库")
    sql = "select *  from bebt"
    df = pd.read_sql_query(sql, engine_ts)
    logging.info(df)