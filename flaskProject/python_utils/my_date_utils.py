import datetime
import time
'''
format
'%Y年%m月%d日'
'%Y-%m-%d %H:%M:%S'
'''
'''当前字符串时间'''
def get_curr_time(format):
    return time.strftime(format, time.localtime())
'''当前时间戳'''
def get_timestamp():
    curr_timestamp=time.time()
    return curr_timestamp
'''时间戳返回字符串'''
def get_time_String(timeStamp,format):
    dateArray = datetime.datetime.utcfromtimestamp(timeStamp)
    time_string = dateArray.strftime(format)
    return time_string
def get_format_timestamp(str_time,format):
    str_time = '2020-12-25'
    format = '%Y-%m-%d'
    return int(time.mktime(time.strptime(str_time, format)))
'''时间戳天数差'''
def count_days(start,end):
    return  (datetime.datetime.fromtimestamp(start) - datetime.datetime.fromtimestamp(end)).days


#判断当前时间是否超过某个输入的时间
def func_gettime(s):
    if time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(time.time()))>s:
        return True
    else:
        return False
def add_time_str(days):
    # 先获得时间数组格式的日期
    threeDayAgo = (datetime.datetime.now() - datetime.timedelta(days=days))
    # 转换为时间戳:
    timeStamp = int(time.mktime(threeDayAgo.timetuple()))
    # 转换为其他字符串格式:
    add_time_curr_time_str = threeDayAgo.strftime("%Y-%m-%d %H:%M:%S")
    return add_time_curr_time_str
def add_time_timestamp(days):
    # 先获得时间数组格式的日期
    threeDayAgo = (datetime.datetime.now() - datetime.timedelta(days=days))
    # 转换为时间戳:
    timestamp = int(time.mktime(threeDayAgo.timetuple()))
    return timestamp

def test():
    print(count_days(1608825600,1606060800))


