'''截取某个字符串之前的字符串'''
def split_before(old_string, split_string):
    return old_string.split(split_string)[0]


'''截取多少个(index:数字)后的字符串'''
def split_after_index(old_string, split_string, index):
    return old_string.split(split_string)[index:]


'''开始到末尾(index:数字)几位'''
def cut_string(old_String, index):
    return old_String[0:-index]

'''返回指定字符在字符串中某个位置'''
def str_index(old_String,string):
    return old_String.index(string)
