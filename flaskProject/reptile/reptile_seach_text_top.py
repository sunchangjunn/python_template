#! /usr/bin/python
#!/usr/bin/env python
# 引入BS库
import requests
from datetime import date, datetime
from bs4 import BeautifulSoup
from bs4 import SoupStrainer
import logging
from smtplib import SMTP
from email.header import Header
from email.mime.text import MIMEText
from apscheduler.schedulers.blocking import BlockingScheduler
import re
import time
import random
logging.basicConfig(level=logging.INFO, format='[%(asctime)s] [%(levelname)s %(filename)s:%(lineno)d]--> %(message)s',filename='test_select_text.log')
def test_select_text():
    logging.info("执行搜索任务")
    news=datetime.today().strftime('%Y-%m-%d')+"搜索(需求)结果:"
    next=''
    url='https://www.baidu.com/s?rtt=1&bsst=1&cl=2&tn=news&ie=utf-8&word=幅涨'
    bai_top_header = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36',
        'Referer': 'http://top.baidu.com/'}
    seach_soup = BeautifulSoup(requests.get(url,timeout=30, headers=bai_top_header).text, 'lxml')  # 把网页解析为BeautifulSoup对象
    # logging.info(requests.get(url,timeout=30, headers=bai_top_header).text)
    news = news + "\n第一页\n"
    for item in seach_soup.find_all(srcid='1599'):
        author = item.find('span', class_='c-color-gray c-font-normal c-gap-right').get_text()
        public_time=item.find('span',class_='c-color-gray2 c-font-normal').get_text()
        news = news + author+'  '+public_time + "\n"
        logging.info(public_time)
        logging.info(item.a['href'])
        logging.info(item.a.get_text())
        news=news+item.a.get_text()+"\n"
        news = news +item.a['href']+"\n"
        time.sleep(5)
    if (seach_soup.find(id='page')):
        time.sleep(1)
        nextpage_url = "https://www.baidu.com" + seach_soup.find(id='page').a['href']
        logging.info(nextpage_url)
        next=next_page(nextpage_url)
    news=news+next
    logging.info(news)



    '''
           邮件发送结果
           '''
    try:
        # 请自行修改下面的邮件发送者和接收者
        sender = '1678609093@qq.com'  # 发送者的邮箱地址
        receivers = ['1833896677@qq.com']  # 接收者的邮箱地址
        smtper = SMTP('smtp.qq.com')
        # 三个参数：第一个为文本内容，第二个 plain 设置文本格式，第三个 utf-8 设置编码
        message = MIMEText(news, 'plain', 'utf-8')
        message['From'] = Header("1678609093@qq.com", 'utf-8')  # 发送者
        message['To'] = Header('1833896677@qq.com', 'utf-8')  # 接收者
        subject = datetime.today().strftime('%Y-%m-%d') + '搜索'
        message['Subject'] = Header(subject, 'utf-8')  # 邮件的标题
        smtper.login(sender, 'mhvtjjjdxqahehhb')  # QQ邮箱smtp的授权码
        smtper.sendmail(sender, receivers, message.as_string())
        logging.info('邮件发送完成!')
    except Exception:
        logging.error("Error: 无法发送邮件"+Exception.args)

def next_page(url):
    bai_top_header = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36',
        'Referer': 'http://top.baidu.com/'}
    seach_soup = BeautifulSoup(requests.get(url,timeout=30, headers=bai_top_header).text, 'lxml')  # 把网页解析为BeautifulSoup对象
    # logging.info(requests.get(url,timeout=30, headers=bai_top_header).text)
    next="\n第二页:\n"
    for item in seach_soup.find_all(srcid='1599'):
        logging.info(item.a['href'])
        logging.info(item.a.get_text())
        author = item.find('span', class_='c-color-gray c-font-normal c-gap-right').get_text()
        public_time = item.find('span', class_='c-color-gray2 c-font-normal').get_text()
        next = next + author + '  ' + public_time + "\n"
        next=next+item.a.get_text()+"\n"
        next = next + item.a['href']+"\n"
    return next





'''
反爬操作,避免固定时间固定内容,致使目标拉黑或法律制裁
'''
def random_time():
    now_time=datetime.today().strftime('%Y-%m-%d %H:%M:%S')
    m = random.randint(11, 30)
    s = random.randint(10, 59)
    random_time_str=now_time[0:14]+str(m)+":"+str(s)
    logging.info(random_time_str)
    scheduler.add_job(test_select_text, 'date', run_date=random_time_str)


#定时任务
scheduler = BlockingScheduler()
scheduler.add_job(random_time, 'cron', month='1-12', day_of_week='0-6', hour='8,23', minute='10')
scheduler.start()





