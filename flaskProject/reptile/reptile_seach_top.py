#! /usr/bin/python
import requests
from datetime import date, datetime
from bs4 import BeautifulSoup
import logging
from smtplib import SMTP
from email.header import Header
from email.mime.text import MIMEText
from lxml import etree
from apscheduler.schedulers.blocking import BlockingScheduler
logging.basicConfig(level=logging.INFO,format='[%(asctime)s] [%(levelname)s %(filename)s:%(lineno)d]--> %(message)s',filename='seach_top.log')
def test_baidu_seach_top():
    '''
    百度搜索排行榜
    :return:
    '''
    baidu_top_url = 'http://top.baidu.com/buzz?b=1&fr=20811'
    bai_top_header = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36',
        'Referer': 'http://top.baidu.com/'}
    # 请求超时时间为30秒
    r = requests.get(baidu_top_url, timeout=30, headers=bai_top_header)
    # 如果状态码不是200，则引发日常
    r.raise_for_status()
    # 配置编码
    r.encoding = r.apparent_encoding
    html = r.text
    seach_massage = "百度热搜榜\n\n"
    soup = BeautifulSoup(html, 'html.parser')
    all_topics = soup.find_all('tr')[1:]

    for each_topic in all_topics:
        topic_times = each_topic.find('td', class_='last')  # 搜索指数
        topic_rank = each_topic.find('td', class_='first')  # 排名
        topic_name = each_topic.find('td', class_='keyword')  # 标题目
        if topic_rank != None and topic_name != None and topic_times != None:
            topic_rank = each_topic.find('td', class_='first').get_text().replace(' ', '').replace('\n', '')
            topic_name = each_topic.find('td', class_='keyword').get_text().replace(' ', '').replace('\n', '')
            topic_times = each_topic.find('td', class_='last').get_text().replace(' ', '').replace('\n', '')
            logging.info('{} {}  {}'.format(topic_rank, topic_name.replace('search', ''),topic_times))
            seach_massage = seach_massage + '{} {}  {}'.format(topic_rank, topic_name.replace('search', ''),topic_times) + "\n"

    '''
    微博搜索排行榜
    '''
    seach_massage=seach_massage+"\n\n微博搜索排行榜\n\n"
    ###网址
    weibo_top_url = "https://s.weibo.com/top/summary?Refer=top_hot&topnav=1&wvr=6"
    ###模拟浏览器
    weibo_top_header = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36'}
    ###获取html页面
    html=etree.HTML(requests.get(weibo_top_url,headers=weibo_top_header).text)
    rank=html.xpath('//td[@class="td-01 ranktop"]/text()')
    affair=html.xpath('//td[@class="td-02"]/a/text()')
    view = html.xpath('//td[@class="td-02"]/span/text()')
    affair=affair[1:]
    for i in range(0, len(affair)):
        logging.info("{0:<3}\t{1:{3}<5}\t{2:{3}>2}".format(rank[i],affair[i],view[i],chr(12288)))
        seach_massage=seach_massage+"{0:<3}\t{1:{3}<5}\t{2:{3}>2}".format(rank[i],affair[i],view[i],chr(12288))+"\n"

    '''
     邮件发送结果
     '''
    try:
        # 请自行修改下面的邮件发送者和接收者
        sender = '1678609093@qq.com'  # 发送者的邮箱地址
        receivers = ['1833896677@qq.com']  # 接收者的邮箱地址
        smtper = SMTP('smtp.qq.com')
        # 三个参数：第一个为文本内容，第二个 plain 设置文本格式，第三个 utf-8 设置编码
        message = MIMEText(seach_massage, 'plain', 'utf-8')
        message['From'] = Header("1678609093@qq.com", 'utf-8')  # 发送者
        message['To'] = Header("1833896677@qq.com", 'utf-8')  # 接收者
        subject = datetime.today().strftime('%Y-%m-%d %H-%M-%S') + ' 热搜榜'
        message['Subject'] = Header(subject, 'utf-8')  # 邮件的标题
        smtper.login(sender, 'mhvtjjjdxqahehhb')  # QQ邮箱smtp的授权码
        smtper.sendmail(sender, receivers, message.as_string())
        logging.info('邮件发送完成!')
    except Exception:
        logging.error("Error: 无法发送邮件")

#定时任务
scheduler = BlockingScheduler()
scheduler.add_job(test_baidu_seach_top, 'cron', hour='7,18', minute='50')
scheduler.start()