#! /usr/bin/python


import requests
from datetime import date, datetime
from bs4 import BeautifulSoup
import logging
from smtplib import SMTP
from email.header import Header
from email.mime.text import MIMEText
import pandas as pd
from apscheduler.schedulers.blocking import BlockingScheduler
logging.basicConfig(level=logging.INFO, format='[%(asctime)s] [%(levelname)s %(filename)s:%(lineno)d]--> %(message)s',filename='netease_news.log')
def test_reptile_new_top():
    news=datetime.today().strftime('%Y-%m-%d')+"新闻:"
    logging.info('开始任务:'+news)
    #财经
    news=news+"\n\n24小时财经新闻:\n"
    cj_url='http://money.163.com/special/002526BH/rank.html'
    header = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36',
        'Referer': 'http://top.baidu.com/'}
    cj_soup = BeautifulSoup(requests.get(cj_url,timeout=30, headers=header).text, 'lxml')  # 把网页解析为BeautifulSoup对象
    cj_df = pd.DataFrame(columns=["title", "href"])
    cj_active=cj_soup.find('div',class_='area-half right')
    new_24h=cj_active.find_all('div',class_="tabContents active")[0]
    for item in new_24h.find_all('a',limit=15):
        if(item):
            cj_df=cj_df.append({'title': item['title'],'href':item['href']}, ignore_index=True)
    for cj_message in cj_df.drop_duplicates(['href']).values:
        news=news+cj_message[0]+"\n"+cj_message[1]+'\n'

    # 科技
    news=news+"\n\n24小时科技新闻\n"
    kj_url = "http://news.163.com/special/0001386F/rank_tech.html"
    kj_soup = BeautifulSoup(requests.get(kj_url,timeout=30, headers=header).text, 'lxml')  # 把网页解析为BeautifulSoup对象
    kj_df = pd.DataFrame(columns=["title", "href"])
    kj_active=kj_soup.find('div',class_='area-half left')
    kj_new_24h=kj_active.find_all('div',class_="tabContents")[1]
    for item in kj_new_24h.find_all('a',limit=20):
        if (item):
            kj_df=kj_df.append({'title': item.string, 'href': item['href']}, ignore_index=True)
    for kj_message in kj_df.drop_duplicates(['href']).values:
        news = news + kj_message[0] + "\n" + kj_message[1] + '\n'
    logging.info(news)



    '''
       邮件发送结果
       '''
    try:
        # 请自行修改下面的邮件发送者和接收者
        sender = '1678609093@qq.com'  # 发送者的邮箱地址
        receivers = ['1833896677@qq.com,crazymisu@126.com']  # 接收者的邮箱地址
        smtper = SMTP('smtp.qq.com')
        # 三个参数：第一个为文本内容，第二个 plain 设置文本格式，第三个 utf-8 设置编码
        message = MIMEText(news, 'plain', 'utf-8')
        message['From'] = Header("1678609093@qq.com", 'utf-8')  # 发送者
        message['To'] = Header("测试", 'utf-8')  # 接收者
        subject = datetime.today().strftime('%Y-%m-%d') + '新闻'
        message['Subject'] = Header(subject, 'utf-8')  # 邮件的标题
        smtper.login(sender, 'mhvtjjjdxqahehhb')  # QQ邮箱smtp的授权码
        smtper.sendmail(sender, receivers, message.as_string())
        logging.info('邮件发送完成!')
    except Exception:
        logging.error("Error: 无法发送邮件")

#定时任务
scheduler = BlockingScheduler()
scheduler.add_job(test_reptile_new_top, 'cron',  day_of_week='0-6', hour='8,19', minute='5')
scheduler.start()