#指数
#!/usr/bin/python
import time
from datetime import date, datetime
import pandas as pd
import tushare as ts
from sqlalchemy import create_engine
import numpy as np
import logging
import matplotlib.pyplot as plt
import warnings
warnings.filterwarnings("ignore")  # 忽略警告
import pylab as mpl  #导入中文字体，避免显示乱码

def test_plot():
    warnings.filterwarnings("ignore")  # 忽略警告
    mpl.rcParams['font.sans-serif'] = ['SimHei']  # 设置为黑体字
    plt.title('这里是标题')
    plt.xlabel('这里是x轴')
    plt.ylabel('这里是y轴')
    x = [1, 2, 3]
    y = [3, 6, 9]
    plt.plot(x, y)  # 绘制折线图
    plt.show()  # 展示图形



engine_ts = create_engine('mysql://root:Scj19890606@129.204.84.140:3306/tushare_code?charset=utf8',pool_recycle=3600,pool_size=10, max_overflow=20)

def test_import_index():
    logging.info("开始任务")
    pro = ts.pro_api('8da5a360251a9cc6ff2f8cd1b937728567a62d427562bbaf54f2659a')
    list = ['SZSE','SSE', 'MSCI', 'CSI','SW','OTH']
    for i in list:
        df = pro.index_basic(market=i,fields='ts_code,name,fullname,market,publisher,index_type,category,base_date,base_point,list_date,weight_rule,desc,exp_date')
        res = df.to_sql('index', engine_ts, index=False, if_exists='append', chunksize=50000)
    # df.to_csv("G:\\指数列表.csv")
        logging.info(df)


def test_index_day_deal():
    logging.info("开始")
    pro = ts.pro_api('8da5a360251a9cc6ff2f8cd1b937728567a62d427562bbaf54f2659a')
    df = pro.index_daily(ts_code='000001.SH', start_date='20210126', end_date='20210126')
    logging.info(df)



def test_index_daily():
    pd.set_option('display.width', 1000)
    pd.set_option('display.max_columns', None)
    # 显示所有行
    pd.set_option('display.max_rows', 10)
    logging.info("开始")
    pro = ts.pro_api('8da5a360251a9cc6ff2f8cd1b937728567a62d427562bbaf54f2659a')
    array = ['399976.SZ', '930997.SZ','399417.SZ']
    for index in array:
        print(index)
        df = pro.index_daily(ts_code=index, start_date='20210114', end_date='20210114',fields='ts_code,trade_date,close,open,high,low,pre_close,change,pct_chg,vol,amount')
        if (not df.empty):
            logging.info(df)
        else:
            logging.error(index+" 为空")



def test_index_dailybasic():
    pro = ts.pro_api('8da5a360251a9cc6ff2f8cd1b937728567a62d427562bbaf54f2659a')
    df = pro.index_dailybasic(trade_date='20181018', fields='ts_code,trade_date,turnover_rate,pe')

def test_index_weight():
    logging.info("开始")
    pro = ts.pro_api('8da5a360251a9cc6ff2f8cd1b937728567a62d427562bbaf54f2659a')
    df = pro.index_weight(index_code='000932.SH', start_date='20180101', end_date='20201119',fields='index_code,con_code,trade_date,weight')
    res = df.to_sql('index_weigth', engine_ts, index=False, if_exists='append', chunksize=50000)

def test_for():
    pd.set_option('display.width', 1000)
    pd.set_option('display.max_columns', 100)
    engine_ts = create_engine('mysql://root:Scj19890606@129.204.84.140:3306/tushare_code?charset=utf8',pool_recycle=3600, pool_size=10, max_overflow=20)
    sql = "select *  from index_daily"
    df = pd.read_sql_query(sql, engine_ts)
    sql_select_indexs = "select *  from `index`"
    indexs_df = pd.read_sql_query(sql_select_indexs, engine_ts)
    indexs_df.set_index("ts_code",inplace=True)
    # logging.info(codes_df)
    # logging.info(codes_df['name'].loc['000002.SH'])

    df.groupby('ts_code')
    for key,group in df.groupby('ts_code'):
        logging.info(key)
        group['index_name']=indexs_df['name'].loc[key]
        logging.info(group)
        logging.info(indexs_df['name'].loc[key])
        logging.info(group.info())
        # np.array(a)

        # df.insert(1, 'index_name_1', np.zeros(group.size),allow_duplicates=False)  # 1表示插入列的位置（索引）， 'd'是列标题

        # logging.info(group.sort_values(by='trade_date',ascending=False))




def test_la():
    data='2020,11,27'
    str_data=lambda  data:data.split(",")
    logging.info(str_data)



