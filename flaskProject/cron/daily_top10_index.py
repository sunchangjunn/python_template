#! /usr/bin/python
import time
from datetime import date, datetime
from apscheduler.schedulers.blocking import BlockingScheduler
import logging
from smtplib import SMTP
from email.header import Header
from email.mime.text import MIMEText
import pandas as pd
import tushare as ts
import pymysql
from sqlalchemy import create_engine
import json
import random


def test_daily_top10_index():
    pro = ts.pro_api('8da5a360251a9cc6ff2f8cd1b937728567a62d427562bbaf54f2659a')
    pd.set_option('display.width', 1000)
    pd.set_option('display.max_columns', None)
    # 显示所有行
    pd.set_option('display.max_rows', 10)
    index_massage = "十大成交股\n"
    params_time = datetime.today().strftime('%Y%m%d')
    df = pro.hsgt_top10(trade_date='20210729', market_type='3')
    # logging.info(df)

    if (not df.empty):
        for index, row in df.iterrows():
            index_message_list = [
                '代码:' + str(row['ts_code']), '  ',
                '名称:' + str(row['name']), '  ',
                '成交金额:' + str(row['amount'])[0:-10] + "亿", '  ',
                '净成交:' + str(row['net_amount'])[0:-6] + "万", '  ',
                '代码:' + str(row['net_amount']), '  ',
                '\n']
            index_massage = index_massage + "".join(index_message_list)
        logging.info(index_massage)
    else:
        logging.error(" 空")
    pass
    df = pro.hsgt_top10(trade_date=params_time, market_type='1')
    # logging.info(df)
    index_massage = index_massage +  "上证十大成交股:\n"
    if (not df.empty):
        for index, row in df.iterrows():
            index_message_list = [
                '代码:' + str(row['ts_code']), '  ',
                '名称:' + str(row['name']), '  ',
                '成交金额:' + str(row['amount'])[0:-10] + "亿", '  ',
                '净成交:' + str(row['net_amount'])[0:-6] + "万", '  ',
                '\n']
            index_massage = index_massage + "".join(index_message_list)
    else:
        logging.error(" 空")
    df = pro.hsgt_top10(trade_date=params_time, market_type='3')
    logging.info(params_time)
    index_massage = index_massage + "深证十大成交股:\n"
    if (not df.empty):
        for index, row in df.iterrows():
            index_message_list = [
                '代码:' + str(row['ts_code']), '  ',
                '名称:' + str(row['name']), '  ',
                '成交金额:' + str(row['amount'])[0:-10] + "亿", '  ',
                '净成交:' + str(row['net_amount'])[0:-6] + "万", '  ',
                '\n']
            index_massage = index_massage + "".join(index_message_list)
    else:
        logging.error(" 空")
    logging.info(index_massage)
