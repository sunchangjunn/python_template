from datetime import date,datetime
from apscheduler.schedulers.blocking import BlockingScheduler
import logging

scheduler = BlockingScheduler()


# def my_job():
#     print(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
#
#
# # 在2019年4月15日执行
# scheduler.add_job(my_job, 'date', run_date=datetime(2020, 11, 11, 14, 0, 10), args=['测试任务'])
# # scheduler.add_job(my_job(), 'interval', hours=2)
# scheduler.start()


def func():
    now = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    logging.error(f'{now} hello world!')



'''
一:
date 是最基本的一种调度，作业任务只会执行一次。它表示特定的时间点触发
'''

scheduler.add_job(func, 'date', run_date='2020-12-22 18:20:05')
'''
二:
周期触发任务:interval
可用参数:
参数	说明
weeks(int)	间隔几周
days(int)	间隔几天
hours(int)	间隔几小时
minutes(int)	间隔几分钟
seconds(int)	间隔多少秒
start_date(datetime or str)	开始日期
end_date(datetime or str)	结束日期
timezone(datetime.tzinfo or   str)	时区
'''
# 每隔 1分钟 运行一次 job 方法
scheduler.add_job(func, 'interval', minutes=1, args=['job1'])
# 在 2019-08-29 22:15:00至2019-08-29 22:17:00期间，每隔1分30秒 运行一次 job 方法
scheduler.add_job(func, 'interval', minutes=1, seconds = 30, start_date='2019-08-29 22:15:00', end_date='2019-08-29 22:17:00', args=['job2'])

'''
三:在特定时间周期性地触发，和Linux crontab格式兼容。它是功能最强大的触发器。
参数	说明
year(int or str)	年，4位数字
month(int or str)	月（范围1-12）
day(int or str)	日（范围1-31）
week(int or str)	周（范围1-53）
day_of_week(int or str)	周内第几天或者星期几（范围0-6或者mon,tue,wed,thu,fri,stat,sun）
hour(int or str)	时（0-23）
minute(int or str)	分（0-59）
second(int or str)	秒（0-59）
start_date(datetime or str)	最早开始日期（含）
end_date(datetime or str)	最晚结束日期（含）
timezone(datetime.tzinfo or   str)	指定时区
'''
scheduler .add_job(func, 'cron', month='1-3,7-9',day='0, tue', hour='0',minute='0',second='0')
scheduler.start()




