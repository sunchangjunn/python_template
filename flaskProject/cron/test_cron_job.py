#! /usr/bin/python
import time
from datetime import date, datetime
from apscheduler.schedulers.blocking import BlockingScheduler
import logging
from smtplib import SMTP
from email.header import Header
from email.mime.text import MIMEText
import pandas as pd
import tushare as ts
import pymysql
from sqlalchemy import create_engine
import json
import random

engine_ts = create_engine('mysql://root:Scj19890606@129.204.84.140:3306/tushare_code?charset=utf8', pool_recycle=3600,
                          pool_size=10, max_overflow=20)
logging.basicConfig(level=logging.INFO, format='[%(asctime)s] [%(levelname)s %(filename)s:%(lineno)d]--> %(message)s',
                    filename='daily_index.log')


def test_get_massage():
    # logging.addLevelName(logging.ERROR, "\033[1;41m%s\033[1;0m" % logging.getLevelName(logging.ERROR))
    now_time = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(time.time()))
    logging.info("开始定时任务:" + now_time)
    pd.set_option('display.width', 1000)
    pro = ts.pro_api('8da5a360251a9cc6ff2f8cd1b937728567a62d427562bbaf54f2659a')
    array = [
        '000001.SH',  # 上证
        '399001.SZ',  # 深证
        '399006.SZ',  # 创业
        '000688.SH',  # 科创50
        '399300.SZ',  # 300
        '399905.SZ',  # 500
        '000906.SH',  # 800
        '000852.SH',  # 1000
        '399610.SZ',  # TMT是科技、媒体和通信三个英文单词的缩写
        '399997.SZ',  # 酒
        '399231.SZ',  # 农业
        '000807.CSI',  # 食品饮料
        '399608.SZ',  # 科技 //http://vip.stock.finance.sina.com.cn/corp/go.php/vII_NewestComponent/indexid/399608.phtml
        '930652.CSI',  # 消费电子
        '399368.SZ',  # 航天军工
        '399913.SZ',  # 医药
        '399395.SZ',  # 有色
        '000987.SH',  # 材料
        '930598.CSI',  # 稀土
        '399986.SZ',  # 银行
        '399975.SZ',  # 证券
        '399809.SZ',  # 保险
        '399417.SZ',  # 新能源车
        '990001.CSI',  # 半导体
        '399974.SZ',  # 国企改革
        '399812.SZ',  # 养老产业
        '000928.SH',  # 能源指数

    ]
    params_time = datetime.today().strftime('%Y%m%d')
    # params_time='20201204'
    # 昨日
    last_time_sql = "select pretrade_date  from trade_cal where is_open=1 and  cal_date=" + params_time
    last_time_df = pd.read_sql_query(last_time_sql, engine_ts)
    last_time = last_time_df['pretrade_date'].iloc[0]
    # 前日
    last_last_time_sql = "select pretrade_date  from trade_cal where is_open=1 and  cal_date=" + last_time
    last__last_time_df = pd.read_sql_query(last_last_time_sql, engine_ts)
    last_last_time = last__last_time_df['pretrade_date'].iloc[0]

    index_massage = params_time + "行情数据\n\n"
    for index in array:
        time.sleep(10)
        logging.info("查询参数:index_code=" + index + " time=" + params_time)
        query_index_name_sql = "select name  from `index`  where ts_code='" + index + "'"
        index_name_df = pd.read_sql_query(query_index_name_sql, engine_ts)
        # 今日
        df = pro.index_daily(ts_code=index, start_date=params_time, end_date=params_time,
                             fields='ts_code,trade_date,close,open,high,low,pre_close,change,pct_chg,vol,amount')
        # 昨日
        last_daily_df = pro.index_daily(ts_code=index, start_date=last_time, end_date=last_time,
                                        fields='ts_code,trade_date,close,open,high,low,pre_close,change,pct_chg,vol,amount')
        # 前日
        last_last_daily_df = pro.index_daily(ts_code=index, start_date=last_last_time, end_date=last_last_time,
                                             fields='ts_code,trade_date,close,open,high,low,pre_close,change,pct_chg,vol,amount')
        if (not df.empty):
            index_message_list = [index_name_df['name'].iloc[0] + ':' + str(df['ts_code'].iloc[0]), '  ',
                                  '收盘' + str(df['close'].iloc[0]).split('.')[0], '  ',
                                  '\n',

                                  '今涨跌幅:' + str(df['pct_chg'].iloc[0])[
                                            0:str(df['pct_chg'].iloc[0]).index('.') + 3] + '%', '  ',
                                  '今成交:' + str(df['amount'].iloc[0])[0:-7] + "亿", '  ',
                                  '\n',

                                  '昨涨跌幅:' + str(last_daily_df['pct_chg'].iloc[0])[
                                            0:str(df['pct_chg'].iloc[0]).index('.') + 3] + '%', '  ',
                                  '昨成交:' + str(last_daily_df['amount'].iloc[0])[0:-7] + "亿", '  ',
                                  '\n',

                                  '前涨跌幅:' + str(last_last_daily_df['pct_chg'].iloc[0])[
                                            0:str(df['pct_chg'].iloc[0]).index('.') + 3] + '%', '  ',
                                  '前成交:' + str(last_last_daily_df['amount'].iloc[0])[0:-7] + "亿", '  ',
                                  # '开盘:'+str(df['open'].iloc[0]).split('.')[0], '  ',

                                  # '昨收:' + str(df['pre_close'].iloc[0]).split('.')[0], '  ',
                                  # '最高:' + str(df['high'].iloc[0]), '  ',
                                  # '最低:'+str(df['low'].iloc[0]),'  ',
                                  '\n', '\n']
            index_massage = index_massage + "".join(index_message_list)
        else:
            logging.error(index + " 空")
            pass

    logging.info(index_massage)

    '''
    邮件发送结果
    '''
    try:
        # 请自行修改下面的邮件发送者和接收者
        sender = '1678609093@qq.com'  # 发送者的邮箱地址
        receivers = ['1833896677@qq.com']  # 接收者的邮箱地址
        smtper = SMTP('smtp.qq.com')
        # 三个参数：第一个为文本内容，第二个 plain 设置文本格式，第三个 utf-8 设置编码
        message = MIMEText(index_massage, 'plain', 'utf-8')
        message['From'] = Header("1678609093@qq.com", 'utf-8')  # 发送者
        message['To'] = Header("测试", 'utf-8')  # 接收者
        subject = params_time + '行情数据'
        message['Subject'] = Header(subject, 'utf-8')  # 邮件的标题
        smtper.login(sender, 'mhvtjjjdxqahehhb')  # QQ邮箱smtp的授权码
        smtper.sendmail(sender, receivers, message.as_string())
        logging.info('邮件发送完成!')
    except Exception:
        logging.error("Error: 无法发送邮件")





# 定时任务
scheduler = BlockingScheduler()
scheduler.add_job(test_get_massage, 'cron', month='1-12', day_of_week='0-4', hour='17', minute='30')
scheduler.start()
