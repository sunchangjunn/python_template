#! /usr/bin/python
import time
from datetime import date, datetime
from apscheduler.schedulers.blocking import BlockingScheduler
import logging
from smtplib import SMTP
from email.header import Header
from email.mime.text import MIMEText
import pandas as pd
import tushare as ts
import pymysql
from sqlalchemy import create_engine
import json
import random

logging.basicConfig(level=logging.INFO, format='[%(asctime)s] [%(levelname)s %(filename)s:%(lineno)d]--> %(message)s')
def test_select_data():
    pro = ts.pro_api('8da5a360251a9cc6ff2f8cd1b937728567a62d427562bbaf54f2659a')
    params_time = datetime.today().strftime('%Y%m%d')
    df = pro.index_daily(ts_code='H30533.CSI', start_date='20210730', end_date='20210730',
                         fields='ts_code,trade_date,close,open,high,low,pre_close,change,pct_chg,vol,amount')
    logging.info(df)