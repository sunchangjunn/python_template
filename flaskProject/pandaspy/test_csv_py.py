import pandas as pd
import tushare as ts
from sqlalchemy import create_engine
import numpy as np
import pytest
import logging

def test_read_csv():
    df_ver=pd.read_csv("F:\\tmp\\iptv_res_ver.csv")
    df_ver.set_index(['vid', 'rid'], inplace=True)
    pd.set_option('display.max_columns', None)
    # 显示所有行
    pd.set_option('display.max_rows', 10)
    pd.set_option('display.width', 1000)
    logging.info(df_ver)
    logging.info(df_ver.index)
    logging.info(df_ver.columns)
    #选取列
    # logging.info(df_ver[['name','keyword']])
    #选取行
    logging.info(df_ver.loc[1:3])
