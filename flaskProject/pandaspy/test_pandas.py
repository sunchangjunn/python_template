import pandas as pd
import tushare as ts
import numpy as np
import logging
from pandas import DataFrame

def test_create_Series():
    #用数组生成
    data=[0,1,2,]
    s = pd.Series(data, index=['a','b','c'])
    #用字典生成
    d = {'b': 1, 'a': 0, 'c': 2}
    dirt = pd.Series(d, index=['a', 'b', 'c'])
    dirt.to_json("G:\\json.text")
    logging.info(s)
    logging.info(dirt)

def test_create_DataFrame():
    data = [0, 1, 2, ]
    d = {'b': 1, 'a': 0, 'c': 2}
    s = pd.Series(data, index=['a', 'b', 'c'])
    dirt = pd.Series(d, index=['a', 'b', 'c'])
    logging.info(s)
    logging.info(dirt)



def test_create_DataFrame_add_ndarray():
    # 显示所有列
    pd.set_option('display.max_columns', None)
    # 显示所有行
    pd.set_option('display.max_rows', None)
    index=['num','ts_code','symbol','name','area','industry','market','list_date']
    test=pd.DataFrame(columns=index)
    df = pd.read_csv("G:\\基础数据.csv")
    logging.info(df.dtypes.value_counts())
    logging.info(type(df.to_numpy))

    # for line in df.values:
    #     if ('深圳' == line[4]):
    #         df_i  = pd.DataFrame([line], columns=index)
    #         test = test.append(df_i)
    # test.to_csv("G:\\深圳基础数据.csv")


def test_pandas_to_csv():
    logging.info("111")
    # 显示所有列
    pd.set_option('display.max_columns', None)
    # 显示所有行
    pd.set_option('display.max_rows', None)
    pro = ts.pro_api()
    df = pro.stock_basic()
    res = df.to_csv("G:\\基础数据.csv")
    logging.info(res)

def test_pandas_read_csv():
    df=pd.read_csv("G:\\基础数据.csv")
    logging.info(type(df.values))
    sz = pd.DataFrame()
    for line in df.values:
        if('深圳' == line[4]):
            print()
            logging.info(type(line))
    logging.info(sz)









