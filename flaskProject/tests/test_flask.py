import os
import tempfile

import pytest

from controller.iptv_config_config import find_id

import flask

app = flask.Flask(__name__)

with app.test_request_context('/?name=Peter'):
    assert flask.request.path == '/'
    assert flask.request.args['name'] == 'Peter'



def test_empty_db(client):
    """Start with a blank database."""
    find_id
    rv = client.get('/')
    assert b'No entries here so far' in rv.data