#!/usr/bin/python
# -*- coding: UTF-8 -*-
import logging
import time  # 引入time模块
from datetime import datetime
from dateutil.relativedelta import relativedelta


def test_get_time():
    now_time = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(time.time()))
    logging.info(now_time)
    ticks = time.time()
    logging.info(ticks)
    # ymd=time.strftime('%Y%m%d', time.localtime(time.time()))
    # logging.info(ymd)
    # logging.info(type(ymd))
    #昨天
    # today = datetime.date.today()
    # yesterday = today - datetime.timedelta(days=1)
    # logging.info(yesterday)
    # #上个月
    # today = datetime.date.today()
    # next_month_day = today + relativedelta(months=1)

    # next_month_day_str=time.strftime('%Y%m%d',next_month_day)
    # logging.info(type(next_month_day))
    # logging.info(next_month_day)

    struct_time = time.localtime(time.time())  # 得到结构化时间格式
    logging.info(type(struct_time))
    now_time = time.strftime("%Y-%m-%d %H:%M:%S", struct_time)
    logging.info(now_time)

def test_time():
    today = datetime.today()
    logging.info(today)
    logging.info(type(today))
    logging.info(datetime.today().strftime('%Y%m%d'))
    logging.info(type(today.strftime('%Y-%m-%d')))

    ccc=datetime.strptime('2018-09-08', '%Y-%m-%d')
    logging.info(ccc)
    logging.info(type(ccc))

    logging.info("今天是:"+today.strftime('%Y-%m-%d')+",类型是:")